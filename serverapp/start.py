#!/usr/bin/python3

# we need to run a thread to catch keybaord keybindings if we want debug output
# so spin the actual server off onto it's own thread, and run the keybinding listening code here


import SDBFM_server
#from threading import Thread
from multiprocessing import Process
import sys, termios, tty, os, time

# code from https://www.jonwitts.co.uk/archives/896
def getch():
  stdio_fd = sys.stdin.fileno()
  old_settings = termios.tcgetattr(stdio_fd)
  char = ''
  try:
    tty.setraw(sys.stdin.fileno())
    char = sys.stdin.read(1)
  #except (KeyboardInterrupt, SystemExit): # this doesn't work because CTRLC+D+Z are characters getting recorded
    #exit() # nope
    #raise
    #sys.exit() # still doesn't work...
  
  finally:
    termios.tcsetattr(stdio_fd, termios.TCSADRAIN, old_settings)
  return char

def print_users():
  global server_thread
  
  user_list = server_thread.user_list
  print(user_list)

def catch_key_loop():
  global server_thread
  SDBFM_server.set_proc_name("SDBFM_keycatch")
  while True:
    char = getch()
    
    if ord(char) < 32: #CTRLC, CTRLD, CTRLZ at least1
      print("Shutting down")
      #server_thread.join()
      server_thread = None
      sys.exit()
    if char == 'p':
      print_users()

if __name__ == "__main__":
  global server_thread
  # first, spin off the server
  #SDBFM_server.run_server()
  #server = SDBFM_server.SDBFM_server()
  #server_thread = Process(target = server.run_server)
  #server_thread.start()
  server = SDBFM_server.SDBFM_server()
  server.run_server()

  # catch keybindings
  catch_key_loop()
