#!/usr/bin/env python

# Copyright 2019 Brian Klaus (isghj)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# problem: we need to know the last location of all users that connect to the server

# users can be known as their username and/or their IP addr

from collections import deque

class SDBFM_Location:

  __slots__ = 'x', 'y', 'angle', 'speed', 'time'

  def __init__(self, x, y, angle, speed, time):
    self.x      = x
    self.y      = y
    self.angle  = angle
    self.speed  = speed
    self.time   = time
  
  def to_string(self):
    return str(self.x) + "lat, " \
        +  str(self.y) + "lon, at "\
        +  str(self.time) #+ " "

class SDBFM_User:

  __slots__ = 'username', 'IP_list', 'location_list'
  # IP_list is deque limited to size 10
  # location_lis tis deque limited to size 20

  #def SDBFM_User(self, uname = "". IP = "", loc = None):
  def __init__(self, uname = "", IP = "", loc = None):
    self.username = uname
    if IP:
      #self.IP_list = [IP]
      self.IP_list = deque(maxlen=10) # we don't need to keep more than the latest IP address
      self.IP_list.append(IP)
    else:
      self.IP_list = deque(maxlen=10)
    if loc:
      self.location_list = deque(maxlen=25)
      self.location_list.append(loc)
      #self.location_list = [loc]
    else:
      self.location_list = deque(maxlen=25)
      #self.location_list = []

  # not sure I ever used this, since it's so simple, and I'm not big on private class vars
  def check_IP(self, unknown_ip):
    return unknown_ip in u_IP_list

  def merge(self, other_user):
    self.IP_list += other_user.IP_list
    self.location_list += other_user.location_list
    #for ip in other_user.IP_list:
      #self.IP_list.append(ip)
    #for loc in other_user.location_list:
      #self.location_list.append(loc)


