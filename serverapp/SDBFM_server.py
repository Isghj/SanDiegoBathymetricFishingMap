#!/usr/bin/env pythonclass

# Copyright 2019 Brian Klaus (isghj)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# server app for showing one user where other users are on my android app
# needs to keep, in memory, the last known locations of every memember that is online

# looks like for now this is python2 only, some string parsing is complaining about byte/string conversions

import sys
import socket
import random
import datetime
from SDBFM_user import *

LISTEN_PORT    = 6114 # random port, no signifigance


# code taken from http://stackoverflow.com/questions/564695/is-there-a-way-to-change-effective-process-name-in-python
# would have been nice if python had a simpler way to do this
#   great... doesn't work with python3
def set_proc_name(newname):
  from ctypes import cdll, byref, create_string_buffer
  libc = cdll.LoadLibrary('libc.so.6')
  buff = create_string_buffer(len(newname)+1)
  #buff.value = newname
  buff.value = newname.encode("utf-8")
  libc.prctl(15, byref(buff), 0, 0, 0)


# this is global level!
# this seems overly complicated where alladdr is just
#[('93.184.216.34', 22), ('2606:2800:220:1:248:1893:25c8:1946', 22, 0, 0)]

def get_global_IP(host,port=80):
  alladdr = list(set(map(lambda x: x[4],socket.getaddrinfo(host,port))))
  ip4 = filter(lambda x: '.' in x[0],alladdr)
  ip6 = filter(lambda x: ':' in x[0],alladdr)

  sprint("listening on: ")
  for addr6,addr4 in zip(ip6, ip4):
    sprint("global IPv4: "+ addr4[0])
    sprint("global IPv6: "+ addr6[0])


# this exists because we still want a valid IPV4 for the user to input, even if we don't care at the api level
# does NOT work for IPV6, just excepts
def get_local_IP(ipv = 4):
  if ipv == 4:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  elif ipv == 6:
    s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
  else:
    sprint("Error: unknown IP version")

  try:
    if ipv == 4:
      s.connect(('1.1.1.1', 22)) # doesn't even have to be reachable
      IP = s.getsockname()[0]
    elif ipv == 6:
      s.connect(('2001:4860:4860::8888', 22, 0, 0)) # doesn't even have to be reachable
      IP = s.getsockname()[0]
      
  except (Exception ):
    sprint("Exeption: ")
    sprint(sys.exc_info())
    IP = '127.0.0.1'
  finally:
    s.close()
  return IP

def sprint(print_str):
  #print(print_str, flush=True)
  print(print_str)
  #sys.stdout.write(print_str)
  sys.stdout.flush()


class SDBFM_server:
  

  def __init__(self):
    self.user_list = []
    self.angler_count = 0

  # the user CAN start transmitting their location before they have a username, and they can change it
  def update_username(self, new_name, ip):
    target_user = None
    for user in self.user_list:
      for old_ip in user.IP_list:
        if old_ip == ip:
          target_user = user
          sprint("user found by IP in previous list:" + user.username)
    if not target_user: # no user found by IP, lets double check username
      for user in self.user_list:
        if new_name == user.username:
          target_user = user
          sprint("user found by username in previous list: " + new_name)
    if not target_user:
      self.user_list.append( SDBFM_User(new_name, ip) )
      sprint("no user found with that name or IP, adding for future")
      sprint("User count:" + str(len(self.user_list)))
    else:
      # TODO: possible bug: angler is never selected first, it is always selected last
      if "Angler" in target_user.username  : # mobile users can move around a lot, lets do a merge if old user
        old_reference = None
        for ip in target_user.IP_list:
          for user in self.user_list:
            if ip in user.IP_list and user != target_user:
              sprint("merging two users because they were the same: " + user.username + " and " + target_user.username)
              user.username = new_name # found old user with same IP, reset his name and hold him
              user.merge(target_user)
              self.user_list.remove(target_user)
              return
      
      target_user.username = new_name # It might just be easier to change it anyway and skip the branch
      if ip not in target_user.IP_list:
        target_user.IP_list.append(ip)

    self.print_all_users()

  def username_lookup(self, ip):
    for user in self.user_list:
      for old_ip in user.IP_list:
        if ip == old_ip:
          return user.username
    sprint("Username lookup failed: " + ip)
    return None

  # takes input user date, their IP addr, and self.user_list
  def update_location(self, data_parts, ip):
    longx     = data_parts[0]
    laty      = data_parts[1]  
    angle     = data_parts[2]
    speed     = data_parts[3]
    time      = data_parts[4]
    sprint(data_parts)
    target_user = None
    for user in self.user_list:
      if ip in user.IP_list:
        target_user = user 
    if not target_user: # need to make a new user 
      new_username = self.get_temporary_username()
      self.user_list.append( SDBFM_User(new_username, ip, SDBFM_Location(longx, laty, speed, angle, time)) )
      sprint("New user with tmp name: "+ new_username + ", User count:" + str(len(self.user_list)))
      sprint("Their IP was:")
    else: # user found, update
      target_user.location_list.append( SDBFM_Location(longx, laty, speed, angle, time))

  # dummy data since I have no users
  def fake_user_data(self):
    #alan = "alan|32.90265033334125|-117.315503077697754|" + str(random.randint(0,359)) + "|10|12:05:05"
    now = datetime.datetime.now()
    alantime = (now - datetime.timedelta(minutes=15)).strftime('%H:%M:%S') # alan is always 15 minutes behind to test grey dot
    toddtime = (now - datetime.timedelta(minutes=5)).strftime('%H:%M:%S') # todd is always 5 minutes behind to test red dot
    alan = "alan|" + str(random.uniform(32.7, 32.9)) + "|-117.315503077697754|" + str(random.randint(0,359)) + "|10|" + alantime
    todd = "todd|" + str(random.uniform(32.7, 32.9)) + "|-117.279052734375|" + str(random.randint(0,359)) + "|30|" + toddtime
    #alan = "alan|" + str(random.uniform(32.7, 32.9)) + "|-117.315503077697754|" + str(random.randint(0,359)) + "|10|12:05:05"
    #todd = "todd|" + str(random.uniform(32.7, 32.9)) + "|-117.279052734375|" + str(random.randint(0,359)) + "|30|15:05:10"
    return "||" + alan + "||" + todd

  # we pass all of our user date back to the client as a long string
  # "user || user || ..."
  # where user is "username|xcord|ycord|angle|speed|time(hour:minute:second)" 
  def encode_group_location_string(self, ignored_name):
    if ignored_name == None:
      sprint("name is null!")
      return
    encode_string = ""
    for user in self.user_list:
      # we only care about users that have location data
      #  also, we want the users to get filtered on the server side, so don't put the users own data back in
      if user.username == ignored_name:
        print("Same user, ignoring: %s : %s" % (user.username, ignored_name))
      if len(user.location_list) > 0 and user.username != ignored_name: 
        #sprint("Username: " + user.username + " has IP list: " )
        #sprint(user.IP_list)
        if len(encode_string) > 0: # only need to separate users if there are more than one
          encode_string += "||"
        last_loc = user.location_list[-1]
        encode_string =  user.username + "|" + last_loc.y + "|" +\
                         last_loc.x + "|" + last_loc.angle + "|" +\
                         last_loc.speed + "|" + last_loc.time
      # if no user location data, don't output
      # for now, we'll only post one location point with a direction, instead of a snake of location points
    encode_string += self.fake_user_data()
    return encode_string 
      
  def print_all_users(self):
    for user in self.user_list:
      sprint("++++++++++++++++++++++++++++++++++++++++++++")
      sprint("Username: " + user.username)
      sprint("--------------------------------------------")
      for ip in user.IP_list:
        sprint("IP:" + ip) 
      for loc in user.location_list:
        sprint("Location:" + loc.to_string()) 
      sprint("++++++++++++++++++++++++++++++++++++++++++++")
   

  def get_temporary_username(self):
    newname = "Angler" + format(self.angler_count, "03") # nobody ever said I was original
    self.angler_count += 1
    return newname

  def run_server(self):
    set_proc_name("SDBFM_server") 
    ip_type = 6
    for param in sys.argv:
      if param == "-6":
        ip_type = 6
      if param == "-4":
        ip_type = 4

    self.user_list = []
    if ip_type == 6:
      s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
      s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
      s.bind(("::", LISTEN_PORT)) # counts as ANY ipv4 address suposidly
    else: # if ip_type == 4:
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
      s.bind(("0.0.0.0", LISTEN_PORT)) # counts as ANY ipv4 address suposidly
    get_global_IP("example.com", 22)
    sprint("local IPv4: " + get_local_IP(4))
    sprint("local IPv6: " +  get_local_IP(6))
    sprint('family: ')
    sprint( s.family)
    sprint('========================= ' )
    #sprint('peername: ' + s.getpeername())
      
    #s.listen(1) # backlog is optional in +3.5, and one is too small, jump to a higher number
    s.listen(100)
   
    while True:
      try:
        conn, addr = s.accept()
        data = conn.recv(64)
        data = data.decode('utf-8')
      except ConnectionResetError:
        continue
      sprint("recieved data: " +  data)
      pieces   = str(data).split("|")
      if ip_type == 4:
        ip, port = conn.getpeername()
      else:
        ip, port, ukn1, ukn2 = conn.getpeername()
      if not data or len(data) == 0:
        sprint("data was empty, not sure why")
        # probably because the other client gave out
      elif len(pieces) == 2 and pieces[1] == "status?":
        #send_status_response( addr )
        sprint("Recieved new user name: " + pieces[0])
        sprint("IP: " + ip)
        self.update_username(pieces[0], ip)
        bytes_string = ("Status good, " + pieces[0])#.encode("utf-8")
        conn.send(bytearray(bytes_string, 'utf8'))
      else:
        if len(pieces) == 5:
          self.update_location(pieces, ip)
          current_username = self.username_lookup(ip)
          bytes_string = self.encode_group_location_string(current_username) # python3 needs encoded bytes
          conn.send(bytearray(bytes_string, 'utf8'))
        else:
          sprint('data was the wrong size: ' + str(len(pieces)) +' pieces')
      conn.close()


