![](images/header.png)

This is a fishing app for android that shows the depth contours of the ocean around La Jolla, California. Some additional areas in southern california are also included (SD north county beaches, Point Loma, SD harbor, dana point, San Clemente, ect)

Note: I have two different sets of data that overlap a lot of orange county / LA. One set is not better than the other, bits and pieces from both are better than the other, although I haven't yet found a good tool to merge the best parts of the data into one good set, so for some areas, one set was chosen over another, somtimes I forgot to render the best one.

Note: if you want data rendered for any particular area in southern california and the data is available to render a map, let me know I can probably render you some new map fragments.

Note: If there's some information you want to add to the map, like artificail reefs and such, email me with coords and information about the land mark and I'll see about adding it to the map

Background:
------
 After the 2015 El Nino year, the La Jolla kelp beds reduced in size significantly due to warmer water. In 2016, The kelp beds that remain were almost all subsurface, and navigating the area from surface kelp became impossible. Without the funds to buy a fishfinder I was left with no reliable way to determine if I was in an area that should have subsurface kelp. Fishing from the bottom, or beyond 20 feet of depth (the visble distance I could see into the water) became imposible without the risk of losing weight/hook/bait snagging on invisible kelp.

So I decided to create a map of the area to help navigate the depths. I found NOAA's data and used QGIS to generate maps and view the data, printing to a lage B size sheet. I decided I was tired of either roughly guessing my exact location on the map or switching back and forth between the map and GPS to get a more accurate reading, so I put it in an android app.

There are two maps, one with depth coloring and including minor contour lines every 5ft for the first 300 ft, and one with greyscale to show the hill shading better without the 5ft contours and includes a kelp overlay, uses data from 1989 because I have trouble getting newer data to integrate with QGIS.

Disclaimer: 
------
I am not a professional cartographer. The data is from NOAA, the Protected Marine Reserves are just lines between the official boundry coords, and the reserves that follow 3 mile boundries are not accurate on the outer boundry. Do not use this for official navigation, I made it for ocean floor viewing and nothing else. This app burns though battery if you leave it on, you can kill it directly by hitting the backbutton twice, I hold no liability if you get stuck on the ocean with a dead phone. This free program comes with no warranty, ect.

Instructions:
------

**You need more than just the APK to run this app. I'm too poor to buy or host a tile server for the maps to be hosted online, so they have to be cached on the phone/tablet's sd card and you have to download them separately.**

You can download the map caches from here(https://mega.nz/#F!vd4FBbSY!W96d3f9a7UhaTh-bbcO_WA), and put them as a zip in /sdcard/osmdroid, if the folder doesn't exist just make a new one. If you have a /sdcard0/osmdroid put it there, same with /emulated/sdcard/osmdroid

You can stack multiple zip files together, and you do not need to un-zip them, just put them in the folder as is. When you open the app and hit the switch button you should see tiles load, if all you see is a grey grid then OSMDroid doesn't see the tile caches and you should double check that they are in the right place.

Warning: some phones have two sdcard folders (one is just a folder on the internal storage for when the SD card is missing) if this happens, you typically need to put the map files in this internal SD card, but probably depends on your phone

Tapping the the back button twice kills the app, because the phone used to kill the battery but most newer phones won't let that happen unless you specifically allow it to run in the background these days.

I consider this app finished as-is; it does what I want (tells me where I am and what depth the water is under me, and a good guess of what the understructure is) extra features are a lower priority and might get added later.

Data limitations:
------
The data I used to draw these ocean floor maps is from NOAA, and can be downloaded here http://www.ngdc.noaa.gov/dem/squareCellGrid/download/3542

I have not modified the base data, only adding some features to identify locations and drawing contours from the data. It's a compilition of multiple data sources in order to cover as much area as possible. Some data sources were more accurate, some used different gathering techniques and technology and it shows in the DEM as very messy image. If there was a better data set available I would gladly use that, but this is the best I had to work with, or at least the best I could find.

If you know of a better data source that I can use for free please email me.

| ![](images/baddata2.png) | ![](images/baddata3.png) |
|:---:|:---:|
| ![](images/baddata4.png) | ![](images/baddata1.png) |

Replicating with QGIS:
------

In the event you want to render your own tiles with added information or a different area like Dana point, I have included instructions to replicate how I made the maps. If there is an area within san diego county that you want me to re-render feel free to email me and I'll see about rendering it, outside of San diego within california might be possible still possible if noaa provides similar data for the region, or if you find me some viable alternative.

I included almost all of the files used in my own QGIS session here: https://mega.nz/#F!vd4FBbSY!W96d3f9a7UhaTh-bbcO_WA

You'll need to download the NOAA San diego data, specifically the 488mb ASCII and extract to the qgis folder, from here: http://www.ngdc.noaa.gov/dem/squareCellGrid/download/3542 and other areas of california are available from the same web site.

If you copy these files to the same directory as the QGIS in the git repo, it should all open for editing without further configuration.

Instructions on fixing/adding/improving are found on the wiki page: https://gitlab.com/Isghj/SanDiegoBathymetricFishingMap/wikis/replicating-with-qgis

Requirements: 
------
Android 4.0 is the min version this should run on (so that more people can use it, and I don't seem to lose anything from doing that), I built it for my own phone which runs 7.0

Android Permissions: 
------
Internet is not actually required if you only use SDBFM tiles and don't have a user server to keep track of your friends. The App shouldn't need internet for anything else, I don't spy on you or your location (unless you use my user server, but I'm not giving that out) and I don't think OSMDroid does either, so you can probably block it with little conscequence if you want to block the permission. I suppose you can block the GPS too but I don't see the point, I build this app specifically to show where I was on the map in realtime...

The storage requirments are required to use the tiles, if you just want the osmdroid map layer, then you should probably switch to osmand which has more features...

Future features on my todo list:
------
 * Ability to change the starting location, which is currently locked to La Jolla
 * Way to turn off GPS logging file, I use it for my own records is never transfered across internet
 * Add more GPS points for artificial reefs
 * Add more land mark buildings (and fix the condo building by getting some better coords)
 * Find a better kelp overlay to help narrow down where the kelp should be, even if it's based on history rather than detection.
 * Find some points to use for making the 3 mile boundries of marine reserves more accurate
 * Figure out why I can't seem to tell OSMdroid to use a different directory to find it's cache files

Tags:
------
Kayak, Kayak fishing, fishing, bottom fishing, la jolla, san diego, scripps, san elijo fault, socal, south california, SMR, SMCA, NOAA, bathymetry, bathymetric, topographic, navigation, android, application, app, osmdroid, gps, fish finder, dem, QGIS, java
