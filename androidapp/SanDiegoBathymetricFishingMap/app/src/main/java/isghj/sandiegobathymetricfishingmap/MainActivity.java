package isghj.sandiegobathymetricfishingmap;

// Copyright 2019 Brian Klaus (isghj)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RotateDrawable;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.api.IMapController;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.TimeZone;


public class MainActivity extends AppCompatActivity {

  private Context                     mContext;
  public  MapView                     mMapView;

  private MapController               mMapController;
  private GpsMyLocationProvider       mGpsProvider;
  private boolean                     mLocationManagerRegistered = false;
  private LocationManager             mLocationManager;
  private LocationListener            mLocationListener;
  private MapEventsOverlay            mPinOverlay;
  private MapEventsReceiver           mPinEventListener;

  private IntentFilter                mScreenFilter;
  private ScreenReciever              mScreenReciever;
  public  Handler                     mServerHandler;

  // layout
  private Button                            mSwitchTileButton;
  private Button                            mPlaceInterestPoint;
  private Button                            mOpenSettingsButton;
  private Button                            mDebugButton;
  private TextView                          mGPSSpeedTextView;
  private TextView                          mServerStatusTextView;

  protected ItemizedIconOverlay<OverlayItem>  currentLocationHistoryIconOverlay;
  protected ItemizedIconOverlay<OverlayItem>  currentLocationPinsIconOverlay;
  protected ItemizedIconOverlay<OverlayItem>  currentLocationAnglerOverlay;

  protected Drawable                         mOldLocationDrawable;
  protected Drawable                         mInterestPointDrawable;
  protected Drawable                         mAnglerPointDrawable;
  //protected Drawable                         mAnglerArrowDrawable;
  protected Bitmap                           mAnglerArrowBitmap;

  // state
  private int                         mTileState;
  private String                      mCachePath;
  private Date                        lastBackPress;
  public  LinkedList<OverlayItem>     mPreviousLocations;
  public  LinkedList<InterestPoint>   mPreviousInterestPoints;
  private LocationClientThread        serverThread;

  public  SharedPreferences           sharedPreferences;


  //   ----   inherited functions   -----   //

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_main);
    //important! set your user agent to prevent getting banned from the osm servers
    org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants.setUserAgentValue(BuildConfig.APPLICATION_ID);
    searchAltCacheDir();

    mPreviousLocations       = new LinkedList<>(); // TODO if you want to load from older files
    mPreviousInterestPoints  = new LinkedList<>();

    mContext = this.getApplicationContext();

    mMapView = (MapView) findViewById(R.id.map);
    if (sharedPreferences.getBoolean("prefEnableOSMLayer", true)) {
      mMapView.setTileSource(TileSourceFactory.MAPNIK); // for now, later we can assume always use our own tiles
      mTileState = 2; // we want the next button to go to color first, then grey
    }else
      mMapView.setTileSource(new XYTileSource("SDBFM-Color", 5, 17, 256, ".jpg", new String[]{}));

    mMapView.setMultiTouchControls(true);
    // this was used to keep track of what zoom level we were at, mostly for intuition, but removed for screen space
    /* mMapView.setMapListener( new DelayedMapListener(new MapListener() {
      @Override
      public boolean onScroll(ScrollEvent event) {
        return false;
      }

      @Override
      public boolean onZoom(ZoomEvent event) {
        mGPSZoomLevel.setText("Zoom:" + event.getZoomLevel());
        return false;
      }
    })); */


    mGPSSpeedTextView = (TextView) findViewById(R.id.tvGPSSpeed);
    mGPSSpeedTextView.setPadding(10,0,10,0); // couldn't find a way to do this in layout for some odd reason
    mGPSSpeedTextView.setText("GPS Searching ...");

    mServerStatusTextView = (TextView) findViewById(R.id.tvServerStatus);
    mServerStatusTextView.setPadding(15,2,15,2);


    IMapController mapController = mMapView.getController();
    mapController.setZoom(13);
    GeoPoint startPoint = new GeoPoint(32.85, -117.273); // la jolla point
    mapController.setCenter(startPoint);

    mLocationListener = new SpeedListener();

    attempt_GPS_ON();

    mOldLocationDrawable    = ResourcesCompat.getDrawable(getResources(), R.drawable.greydot, null);
    mInterestPointDrawable  = ResourcesCompat.getDrawable(getResources(), R.drawable.yellowdot, null);
    mAnglerPointDrawable    = ResourcesCompat.getDrawable(getResources(), R.drawable.reddot, null);
    //mAnglerArrowDrawable    = ResourcesCompat.getDrawable(getResources(), R.drawable.direction3red, null);
    mAnglerArrowBitmap      = BitmapFactory.decodeResource(this.getResources(), R.drawable.direction3red);

    ScaleBarOverlay mScaleBarOverlay = new ScaleBarOverlay(mMapView);
    mScaleBarOverlay.setCentred(true);
    mScaleBarOverlay.setUnitsOfMeasure(ScaleBarOverlay.UnitsOfMeasure.nautical);



    DisplayMetrics displaymetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    int height  = displaymetrics.heightPixels;
    int width   = displaymetrics.widthPixels;
    
    // seems like a good ratio, but needs the screen size to work
    mScaleBarOverlay.setScaleBarOffset(width / 2, height / 20);
    mMapView.getOverlays().add(mScaleBarOverlay);

    currentLocationHistoryIconOverlay = new ItemizedIconOverlay<>( new LinkedList<OverlayItem>(), mOldLocationDrawable, oldLocationGestureListener, mContext);
    currentLocationPinsIconOverlay    = new ItemizedIconOverlay<>( new LinkedList<OverlayItem>(), mInterestPointDrawable, interestpointIconGestureListener, mContext);
    currentLocationAnglerOverlay      = new ItemizedIconOverlay<>( new LinkedList<OverlayItem>(), mAnglerPointDrawable, anglerIconGestureListener, mContext);
    mMapView.getOverlays().add(currentLocationHistoryIconOverlay);
    mMapView.getOverlays().add(currentLocationPinsIconOverlay);
    mMapView.getOverlays().add(currentLocationAnglerOverlay);

    //this button switches the map tile set of the map on the screen
    mSwitchTileButton = (Button) findViewById(R.id.bButtonSwitch);
    mSwitchTileButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        Toast.makeText(mContext, "Switching Maps ...", Toast.LENGTH_LONG).show();
        boolean OSMEnabled = sharedPreferences.getBoolean("prefEnableOSMLayer", true);
        //mTileState = mTileState == 0 ? 1 : 0;
        mTileState = (mTileState + 1) %  (OSMEnabled ? 3 : 2);
        //prefEnableOSMLayer
        if (mTileState == 0) {
          mMapView.setUseDataConnection(true);
          mMapView.setTileSource(new XYTileSource("SDBFM-Color", 7, 17, 256, ".jpg", new String[]{}));

        }else if (mTileState == 1){
          mMapView.setUseDataConnection(false);
          mMapView.setTileSource(new XYTileSource("SDBFM-Greyscale", 7, 17, 256, ".jpg", new String[]{})); // TODO these need to be updated

        }else {
          mMapView.setUseDataConnection(true);
          mMapView.setTileSource(TileSourceFactory.MAPNIK); // for now, later we can assume always use our own tiles
        }
      }
    });
    mPlaceInterestPoint = (Button) findViewById(R.id.bButtonPlaceInterestPoint);
    mPlaceInterestPoint.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        // if location is missing, throw toast saying cannot get location yet

        if (mGpsProvider == null) {
          toast("GPS hasn't resolved yet, cannot save");

        }else {
          Location temp_loc = mGpsProvider.getLastKnownLocation();
          if (temp_loc == null) {
            Toast.makeText(mContext, "GPS hasn't found your location yet!", Toast.LENGTH_LONG).show();
          } else {
            Date time_now = new Date();
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss a", Locale.US);

            GeoPoint tmp_gp = new GeoPoint(temp_loc);
            OverlayItem temp_overlay = new OverlayItem("New Point", df.format(time_now), tmp_gp);
            temp_overlay.setMarker(mInterestPointDrawable);
            temp_overlay.setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
            mPreviousInterestPoints.add(new InterestPoint(tmp_gp, time_now));
            currentLocationPinsIconOverlay.addItem(temp_overlay);
            mMapView.invalidate(); // needed otherwise the points aren't drawn until you move the map
          }
        }
      }
    });

    mOpenSettingsButton = (Button) findViewById(R.id.bButtonSettings);
    mOpenSettingsButton.setOnClickListener( new View.OnClickListener() {
      public void onClick(View v){
        Intent i = new Intent(mContext, MainPreferenceActivity.class);
        startActivity(i);
      }
    });

    mDebugButton = (Button) findViewById(R.id.bButtonDebug);
    mDebugButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v){
        if (serverThread == null){
          startServerUpdating();
        }else{
          stopServerUpdating();
        }
      }
    });

    mPinEventListener = new MapEventsReceiver() {
      @Override
      public boolean singleTapConfirmedHelper(GeoPoint g) {
        Log.d("SDBFM", "Single tap helper, doing nothing");
        return false;
      }

      @Override
      public boolean longPressHelper(GeoPoint g) {
        Log.d("SDBFM", "LongPressHelper");
        Date time_now = new Date();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss a", Locale.US);

        OverlayItem temp_overlay = new OverlayItem("New Point", df.format(time_now), g);
        temp_overlay.setMarker(mInterestPointDrawable);
        mPreviousInterestPoints.add(new InterestPoint(g,time_now));
        currentLocationPinsIconOverlay.addItem(temp_overlay);
        mMapView.invalidate(); // needed otherwise thepoints aren't drawn until you move the map
        return true;
      }
    };
    mPinOverlay = new MapEventsOverlay(mPinEventListener);
    mMapView.getOverlays().add(mPinOverlay);

    // detect if user turns screen on/off, since onpause doesn't catch this I guess
    mScreenFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
    mScreenFilter.addAction(Intent.ACTION_SCREEN_OFF);
    mScreenReciever = new ScreenReciever();
    registerReceiver(mScreenReciever,mScreenFilter);

    mServerHandler = new Handler(Looper.getMainLooper()){
      @Override
      public void handleMessage(Message inputMessage){
        Bundle bundle       = inputMessage.getData();
        String statusString = bundle.getString("status");
        if (statusString != null && statusString.length() > 0){
          setmServerStatusTextView(statusString);
          return;
        }

        String returnString = bundle.getString("data");
        if (returnString != null){
          Log.d("SDBFM", "(handler)return string: " + returnString);
          decodeServerData(returnString);
          return;
        }
        Log.d("SDBFM", "(handler) unknown message: " + inputMessage.toString());
      }
    };


  } // onCreate end


  @Override
  protected void onPause() {
    super.onPause();
    saveCurrentLocation();
    if (sharedPreferences.getBoolean("prefEnableConstantGPS", false) == false) {
      // if not constant GPS, we need to stop GPS server here
      stopGPSService();
    }
    stopServerUpdating();
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (sharedPreferences.getBoolean("prefEnableConstantGPS", false) == false || mLocationManagerRegistered == false){
      // if not constant GPS, then we need to restart the GPS server here
      startGPSService();
    }
    //    mMapView.getOverlays().add(mScaleBarOverlay);
    //ViewGroupOverlay list = mMapView.getOverlay();
    if (mPreviousLocations == null){
      Log.d("SDBFM","Previous locations array was NULL, we cannot put previous locations on screen");
    }
    else if (mPreviousLocations.size() == 0){
      Log.d("SDBFM","Previous locations size is zero");
    }


    if (mPreviousLocations != null && mPreviousLocations.size() > 0) { //mNumbermPreviousLocationsDisplayed
      //list.remove((View)mPreviousLocations.get(mNumbermPreviousLocationsDisplayed));
      LinkedList<OverlayItem> new_overlay_list = new LinkedList<>();
      new_overlay_list.add(mPreviousLocations.get(0));
      if (mPreviousLocations.size() > 1)
        new_overlay_list.add(mPreviousLocations.get(1));
      if (mPreviousLocations.size() > 2)
        new_overlay_list.add(mPreviousLocations.get(2));
      if (new_overlay_list.size() > 0) {
        currentLocationHistoryIconOverlay = new ItemizedIconOverlay<OverlayItem>(new_overlay_list,
                mOldLocationDrawable,
                oldLocationGestureListener,
                mContext);
        Log.d("SDBFM", "We have this many locations saved: " + mPreviousLocations.size()
                + " and this many overlay_list" + new_overlay_list.size());
        //mMapView.invalidate(); <- removed in 2 to test why previous location dots are missing
      }
    }
    startServerUpdating();
  }

  @Override
  public void onBackPressed() {
    Date now = new Date();
    // if we haven't pressed the button in the last 3 seconds
    if (lastBackPress == null || (now.getTime() - lastBackPress.getTime() > (3*1000)) ) {
      Toast.makeText(mContext, "Press Back again to close the App", Toast.LENGTH_LONG).show();
      lastBackPress = now;
    } else{
      saveVolitileData(); // since this doesn't get called twice, I'm assuming system.exit ignores onDestroy
      System.exit(0); // this is temporary, a fix to the app not letting go of GPS, a clean way to forcibly kill the app to avoid battery death
    }
  }

  @Override
  public void onDestroy() {
    saveVolitileData();
    super.onDestroy();
  }

  // *********************** //
  // --- other functions --- //
  // *********************** //

  private void saveVolitileData(){
    //DateFormat fileformat = new SimpleDateFormat("yyyy-MM-dd_HH-mm"); // file system safe
    Date currentDate = new Date();
    String time_now = new SimpleDateFormat("yyyy-MM-dd_HH-mm", Locale.US).format(currentDate);
    File f = new File(mCachePath + "/" + time_now + ".log");
    try {
      f.createNewFile();
      BufferedWriter out = new BufferedWriter( new FileWriter(f));
      DateFormat readableFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US); // more readable
      time_now = readableFormat.format(currentDate);
      out.write("SDBFM data from " + time_now + ":\n");
      readableFormat = new SimpleDateFormat("HH:mm:ss", Locale.US); // we don't really need the date for each point
      if (mPreviousInterestPoints.size() > 0){
        out.write("points of interest noted:\n");
        for (InterestPoint i : mPreviousInterestPoints){
          time_now = readableFormat.format(i.iCreationTime);
          out.write(time_now + ": " + i.iGeopointLocation.toString() + "\n");
        }
      }
      if (currentLocationHistoryIconOverlay.size() > 0) {
        out.write("locations where the app was closed:\n");
        OverlayItem tmp;
        // where we are right now, we want that in our log too.
        // fewer lines of code to add it last second, than to split the code into loop and aft
        Location tmp_loc = mGpsProvider.getLastKnownLocation();
        if (tmp_loc != null){
          currentLocationHistoryIconOverlay.addItem(new OverlayItem("Point",
                  new SimpleDateFormat("HH:mm:ss a", Locale.US).format(new Date()),
                  new GeoPoint(tmp_loc)));
        }
        int i = 0;
        while( i < currentLocationHistoryIconOverlay.size()){
          tmp = currentLocationHistoryIconOverlay.getItem(i);
          time_now = tmp.getSnippet();
          out.write(time_now + ": " + tmp.getPoint().toString() + "\n");
          i += 1;
        }
      }else{
          out.write("There were no previous location history locations to save");

      }
      out.flush();
      out.close();
      Log.d("SDBFM", "file writing complete, checkout @ " + mCachePath);

      }catch (IOException e){
      Log.d("SDBFM", "IO EXCEPTION, now what? tried @" + mCachePath);
    }
  }

  private void saveCurrentLocation(){
    if (mGpsProvider != null) {
      if (mPreviousLocations == null) {
        Log.d("SDBFM", "mPreviousLocations is null");
      } else {
        Location temp_loc = mGpsProvider.getLastKnownLocation();
        if (temp_loc == null) {
          Log.d("SDBFM", "location is null, gps didn't have fix, cannot save");
        } else {
          GeoPoint temp_pnt = new GeoPoint(temp_loc);
          mPreviousLocations.add(new OverlayItem("Previous location", new SimpleDateFormat("HH:mm:ss a", Locale.US).format(new Date()), temp_pnt));
          Log.d("SDBFM", "location added to previous locations");

        }
      }
    }
  }
  
  private void searchAltCacheDir(){
    File f;
    // TODO: Fix this, it doesn't find the SD card folder
    // the reason I'm hardcoding these values is because you wanted to standardize
    //  devices having no SD card so you made an emulated folder on the internal flash.
    //  I DONT WANT INTERNAL I WANT SD CARD,
    //  so fuck you, google
    String newSDCardLocation = Environment.getExternalStorageDirectory().getPath();
    for(String s : new String[]{newSDCardLocation, "/storage/sdcard1", "/storage/sdcard", "/media/sdcard"} ){
      f = new File(s + "/osmdroid");
      Log.d("SDBFM", "dir:" + s);
      if (f.isDirectory()) {
        Log.d("SDBFM", "found new directory: " + s);
        //org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants.setCachePath(f.getPath()); fuck this doesn't worn in new version
        mCachePath = f.getPath();
        return;
      }
      mCachePath = getCacheDir().getPath(); // otherwise
    }
  }

  private void startGPSService() {
    Log.d("SDBFM", "starting gps service...");
    try {
      mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
      mLocationManagerRegistered = true;
    }catch (SecurityException e){
      mGPSSpeedTextView.setText("GPS DENIED");
    }
  }

  private void stopGPSService() {
    Log.d("SDBFM", "stopping gps service...");
    if (mLocationManager != null){
      mLocationManager.removeUpdates(mLocationListener);
      mLocationManagerRegistered = false;
    }
  }

  public void startServerUpdating() {
    // we need to recheck the server every so many seconds, so it can update our points
    boolean serverEnabled = sharedPreferences.getBoolean("prefEnableServer", false);
    if (serverEnabled) {
      if (serverThread == null) {
        String username = sharedPreferences.getString("prefServerUsername", null);
        Log.d("SDBFM", "Username was");
        if (username != null) {
          // call async status
          Log.d("SDBFM", "Username was: " + username);
          new ContactServerTask(this).execute();
          mServerStatusTextView.setVisibility(TextView.VISIBLE);
          mServerStatusTextView.setText("Searching ...");
        } else {
          Log.d("SDBFM", "Username is not set yet");
        }


        Log.d("SDBFM", "starting server");
        //serverThread = new LocationClientThread("192.168.29.82", 6114, this); // works but static
        serverThread = new LocationClientThread(this);
      }
      serverThread.start();

    }else { // server is not enabled
      mServerStatusTextView.setVisibility(TextView.GONE);
    }
  }

  public void stopServerUpdating(){
    // stop getting data from servers, and remove points?
    if (serverThread != null){
      Log.d("SDBFM", "stopping server");
      serverThread.interrupt();
      serverThread = null;
    }
  }

  // we get our data back from the server as a concat string of data,
  // in addition to breaking it into useable data, we need to update the screen data locations
  private void decodeServerData(String server_response) {
    // the server mashes location data for every user on the server within range of our user
    //  where every user has a user name and 5 data pieces, separated by user with double pipe
    // x | y | angle | speed | time

    String[] users = server_response.split("\\|\\|");
    for (String user : users) {
      String[] split_data = user.split("\\|");

      if (split_data.length != 6) {
        Log.d("SDBFM", "Bad data from server:" + user);
      } else {
        // we need to check if the user already exists, if not, add them
        if (currentLocationAnglerOverlay.size() == 0) { // empty, no point checking just add
          addAngler(user);
        } else { // it's not empty, search it
          boolean foundUser = false;
          for (int i = 0; i < currentLocationAnglerOverlay.size(); i++) {
            String itemUsername = currentLocationAnglerOverlay.getItem(i).getTitle();
            //Log.d("SDBFM", "Checking angler: " + itemUsername + " against:" + split_data[0]);
            if (itemUsername.equals(split_data[0])) {
              //Log.d("SDBFM", "User already known, updating...");
              currentLocationAnglerOverlay.removeItem(i); // I didn't see a way to just update the location, so making a new one
              addAngler(user);
              foundUser = true;
              break;
            }
          }
          if (!foundUser) {
            Log.d("SDBFM", "User never seen before, adding...");
            addAngler(user);
          }

          mMapView.invalidate(); // points will update without invalidate, but its slow and looks buggy
        }
      }
    }
  }

  // sure would be nice if there was  a way to rotate drawables, or icons, or overlayitems, or ANYTHING
  // https://stackoverflow.com/questions/20519695/rotate-a-marker-in-osmdroid-for-android
  protected Bitmap RotateMyBitmap(Bitmap source, double angle){
    Matrix m = new Matrix();
    m.postRotate((float) angle);
    return Bitmap.createBitmap(source, 0, 0 , source.getWidth(), source.getHeight(), m, true);
  }

  protected void addAngler(String encodedUserString){
    String[] split_data = encodedUserString.split("\\|");
    // username | x | y | angle | speed | time

    GeoPoint angler_geopoint = new GeoPoint(Double.parseDouble(split_data[1]), Double.parseDouble(split_data[2]));
    OverlayItem temp_overlay = new OverlayItem(split_data[0], split_data[5], angler_geopoint);

    // change the drawable of the angler icon, to grey, if the data for them is really old
    //SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);
    try{
      Calendar now = Calendar.getInstance(TimeZone.getTimeZone("America/Los_Angeles"), Locale.US);
      Calendar last_seen = Calendar.getInstance(TimeZone.getTimeZone("America/Los_Angeles"), Locale.US);
      String time_pieces[] = split_data[5].split(":");
      //Log.d("SDBFM", split_data[5]);
      last_seen.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time_pieces[0]));
      last_seen.set(Calendar.MINUTE, Integer.parseInt(time_pieces[1]));
      last_seen.set(Calendar.SECOND, Integer.parseInt(time_pieces[2]));
      long minutes_since = (now.getTimeInMillis() - last_seen.getTimeInMillis()) / 1000 / 60  ; // getTime returns miliseconds
      if (minutes_since >= 10){ // old data
        temp_overlay.setMarker(mOldLocationDrawable); // grey dot
      }else{
        double speed = Double.parseDouble(split_data[4]);
        if(speed >= 1.0){         //if they move fast enough, give them a drawable arrow pointed in the direction they are going
          Bitmap rotated_bitmap = RotateMyBitmap(mAnglerArrowBitmap, Double.parseDouble(split_data[3]));
          Drawable rotated_drawable = new BitmapDrawable(getResources(), rotated_bitmap);
          temp_overlay.setMarker(rotated_drawable);
        }else { // too slow, just use a dot
          temp_overlay.setMarker(mAnglerPointDrawable); // red dot
        }
      }
    } catch (Exception e){
      // rare outside of old testing data, but sometimes the time is in a bad format, just use a red dot for now
      Log.d("SDBFM","Exception: " + e.toString());
      temp_overlay.setMarker(mAnglerPointDrawable); // red dot
    }

    currentLocationAnglerOverlay.addItem(temp_overlay);
  }

  // returns a location or NULL
  public Location getCurrentLocation() {
    if (mGpsProvider != null) {
      return mGpsProvider.getLastKnownLocation();
    } else
      return null;
  }

  private void attempt_GPS_ON(){
    // headache we can avoid by just ignoring API > 22
    //if (this.checkPermission("android.permission.ACCESS_FINE_LOCATION", "isghj.sandiegobathymetricfishingmap") == PackageManager.PERMISSION_GRANTED) {
    try{
      mLocationManager = (LocationManager) getSystemService(mContext.LOCATION_SERVICE);
      mLocationManager.addGpsStatusListener(new GPSStatusListener());
      mGpsProvider = new GpsMyLocationProvider(mContext);
      mGpsProvider.setLocationUpdateMinTime(1200);
      MyLocationNewOverlay mLocationOverlay = new MyLocationNewOverlay(mGpsProvider, mMapView);
      mLocationOverlay.setDrawAccuracyEnabled(true);
      mLocationOverlay.disableFollowLocation();
      mLocationOverlay.setDirectionArrow(((BitmapDrawable) getResources().getDrawable(R.drawable.loweredwhitedot)).getBitmap(),
              ((BitmapDrawable) getResources().getDrawable(R.drawable.direction3)).getBitmap());
      mLocationOverlay.enableMyLocation();
      mMapView.getOverlays().add(mLocationOverlay);
      startGPSService();
    }catch (SecurityException e){
      toast("GPS was denied.");
      mGPSSpeedTextView.setText("GPS DISABLED");
    }

  }

  private void resetGPSProvider(){
    // problem: if you start the app with GPS disabled sometimes the listener will say we have a fix,
    // but the GPD provider will be unable to get a location, our dot will never show, location will not save to file
    // so this function is our attempt to try to fix that when the error appears


    //mGpsProvider = new GpsMyLocationProvider(mContext);
    mGpsProvider.setLocationUpdateMinTime(1200);

  }

  public void setmServerStatusTextView(String text){
    boolean error = text.contains("Err:");
    if (error){
      mServerStatusTextView.setTextColor(0xff000000); // Black
      mServerStatusTextView.setBackgroundColor(0xffff2435); // RED
      String[] pieces = text.split(": ");
      if (pieces.length == 2)
        text = pieces[1];
    }else { // no error, color by latency
      mServerStatusTextView.setBackgroundColor(0xffd1eaff); // very light blue
      int i = Integer.parseInt(text);
      text += "ms";
      if (i < 100)
        mServerStatusTextView.setTextColor(0xff000000); // black
      else if (i < 200)
        mServerStatusTextView.setTextColor(0xff005094); // blue
      else if (i < 300)
        mServerStatusTextView.setTextColor(0xff005c10); // green
      else if (i < 500)
        mServerStatusTextView.setTextColor(0xffa36400); // orange
      else //if (i < 1000)
        mServerStatusTextView.setTextColor(0xffa30d00); // red
    }

    if (text == null || text == "") {
      //mServerStatusTextView.setEnabled(false);
      mServerStatusTextView.setVisibility(View.GONE);
    }else {
      //mServerStatusTextView.setEnabled(true);
      mServerStatusTextView.setVisibility(View.VISIBLE);
      mServerStatusTextView.setText(text);
    }
  }

  // shorter, easier to read, function call to replace verbose mess
  private void toast(String txt){
    Toast.makeText(getApplicationContext(), txt, Toast.LENGTH_LONG).show();
  }

  // ***************************** //
  // --- reimplemented classes --- //
  // ***************************** //

  org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener interestpointIconGestureListener
          = new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
    @Override
    public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
      toast("time: " + item.getSnippet());
      return true;
    }
    @Override
    public boolean onItemLongPress(final int index, final OverlayItem item) {
      toast( "Hiding point ...");
      item.setMarker(ResourcesCompat.getDrawable(getResources(), R.drawable.emptyasset, null));
      return false;
    }
  };  
  
  org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener oldLocationGestureListener
          = new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
    @Override
    public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
      toast( "time: " + item.getSnippet());
      return true;
    }
    @Override
    public boolean onItemLongPress(final int index, final OverlayItem item) {
      toast( "Hiding point ...");
      item.setMarker(ResourcesCompat.getDrawable(getResources(), R.drawable.emptyasset, null));
      return false;
    }
  };

  // anglerIconGesterListener
  org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener anglerIconGestureListener
          = new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
    @Override
    public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
      // change the drawable of the angler icon, to grey, if the data for them is really old
      //SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);
      try{
        //Date now        = new Date();
        //Date last_seen  = format.parse(item.getSnippet());
        Calendar now = Calendar.getInstance();
        Calendar last_seen = Calendar.getInstance();
        String time_pieces[] = item.getSnippet().split(":");
        last_seen.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time_pieces[0]));
        last_seen.set(Calendar.MINUTE, Integer.parseInt(time_pieces[1]));
        last_seen.set(Calendar.SECOND, Integer.parseInt(time_pieces[2]));
        int  minutes_since = (int) ((now.getTimeInMillis() - last_seen.getTimeInMillis()) / 1000.0 / 60.0)  ; // getTime returns miliseconds
        Location our_location = getCurrentLocation();
        if (minutes_since >= 60  ){ // time has passed, data is old
          String  hours_since   = (minutes_since / 60) + ":" + String.format("%02x", minutes_since % 60);
          toast(item.getTitle() + " was last seen " + hours_since + " hours ago.");

        }else if (minutes_since >= 2 || our_location == null ) { // time has passed, data is old
          toast(item.getTitle() + " was last seen " + minutes_since + " minutes ago.");

        }else{
          IGeoPoint their_igeopoint = item.getPoint();
          Location  their_location  = new Location(our_location);
          their_location.setLatitude(their_igeopoint.getLatitude());
          their_location.setLongitude(their_igeopoint.getLongitude());
          float distance_in_meters = their_location.distanceTo(our_location);
          // TODO make this print to other units later
          toast(item.getTitle() + ", " + minutes_since + " minutes ago, was " + distance_in_meters + " meters away from you");

        }
      } catch (Exception e){
        // rare outside of old testing data, but sometimes the time is in a bad format, just give the time as a string
        Log.d("SDBFM","Exception: " + e.toString());
        toast( item.getTitle() + " at " + item.getSnippet());
      }
      // TODO: calculate the distance between you and them
      return true;
    }
    @Override
    public boolean onItemLongPress(final int index, final OverlayItem item) {
      toast( "Hiding point ...");
      item.setMarker(ResourcesCompat.getDrawable(getResources(), R.drawable.emptyasset, null));
      return false;
    }
  };


  public class SpeedListener implements LocationListener {
    @Override
    public void onLocationChanged(Location loc) {
      double lastSpeed = new BigDecimal(loc.getSpeed()*2.23694).setScale(2, RoundingMode.HALF_UP).doubleValue();
      mGPSSpeedTextView.setText(Double.toString(lastSpeed) + " MPH"); // converts to MPH
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {    }
    @Override
    public void onProviderEnabled(String s) {
      mGPSSpeedTextView.setText("GPS Searching...");
    }
    @Override
    public void onProviderDisabled(String s) {
      mGPSSpeedTextView.setText("GPS is Disabled");
    }
  };

  public class GPSStatusListener implements GpsStatus.Listener {
    @Override
    public void onGpsStatusChanged(int i) {
      if (i == GpsStatus.GPS_EVENT_FIRST_FIX ){
        Location current_loc = mGpsProvider.getLastKnownLocation();
        if (current_loc != null) {
          Toast.makeText(MainActivity.this, "GPS Location found", Toast.LENGTH_SHORT).show();
          // move the screen to the user if user is not within 5km of SDBFM
          mMapView.getController().setCenter(new GeoPoint(current_loc));
        } else {
          Log.d("SDBFM", "first fix being found was announced but cannot get location");
          mGPSSpeedTextView.setText("RESET FOR GPS");
          resetGPSProvider();
        }
      }
      //if (i == GpsStatus.GPS_EVENT_SATELLITE_STATUS){
        // weird... once we put this in GPS started working again...
        //Log.d("SDBFM", "satellite status...");
        //Location current_loc = mGpsProvider.getLastKnownLocation();
        //if (current_loc != null) {
          //Log.d("SDBFM", "We have location!");

        //}
      //}
    }
  }

  public class InterestPoint {
    // all the information about a point the user put on the map
    Date      iCreationTime;
    GeoPoint  iGeopointLocation;

    //   Might need more information

    Boolean   iDeletedFlag;             // has this been marked for delete by user
    int       iUncertaintyRadius;       // what is the guessed radius of the interest from this point
    String    iCommentString;           // if the user wants to add something as a string

    public InterestPoint(GeoPoint g, Date d){
      if (d == null){
        this.iCreationTime = new Date();
      }else{
        this.iCreationTime = d;
      }
      this.iGeopointLocation = g;
      this.iDeletedFlag = false;
      this.iUncertaintyRadius = 50;
    }
  };

  // we want to catch when the screen turns on or off, that is caught here
  private class ScreenReciever extends BroadcastReceiver{

    public boolean wasScreenOn = true; // might not get used, good know though
    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF) && wasScreenOn) {
        saveCurrentLocation();
        //stopGPSService();
        Log.d("SDBFM", "screen off detected");
        wasScreenOn = false;
      } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON) && !wasScreenOn) {
        //startGPSService();
        wasScreenOn = true;
      }
    }
  }

};


