package isghj.sandiegobathymetricfishingmap;

public class Angler {
  String username = "";
  Double longx    = 0.0;
  Double laty     = 0.0;
  Double angle    = 0.0; // could be an int, do we really need single precision angles?
  Double speed    = 0.0;

  Angler(String uname, Double x , Double y, Double a, Double s){
    username = uname;
    longx    = x;
    laty     = y;
    angle    = a; // could be an int, do we really need single precision angles?
    speed    = s;
  }

}
