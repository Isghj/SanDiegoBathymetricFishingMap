package isghj.sandiegobathymetricfishingmap;

// Copyright 2019 Brian Klaus (isghj)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class ContactServerTask extends AsyncTask<String, Integer, String> {

  private MainActivity  mainActivity;
  private int       messageType;

  ContactServerTask(Activity act){
    mainActivity = (MainActivity) act;
  }

  // main
  // two string inputs: text to send, IP as string
  protected String doInBackground(String... args) {
    PrintWriter     out;
    BufferedReader  in;
    Socket          sock;

    //String send_message = args[0];

    try {
      sock = new Socket();
      sock.bind(new InetSocketAddress(6114));
      //sock.connect(new InetSocketAddress("192.168.29.82", 6114), 5000); // socket
      String server_addr = mainActivity.sharedPreferences.getString("prefServerIP", "192.168.29.82");
      // TODO separate a port if the user wants a second port location
      sock.connect(new InetSocketAddress(server_addr, 6114), 5000); // socket
      out = new PrintWriter(sock.getOutputStream(), true);
      in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
    } catch (BindException e) {
      Log.d("SDBFM", "Bind exception, Address was already in use");
      // don't think users need to know about this one, it seems to solve itself the second update
      return null;

    } catch (SocketTimeoutException e){
      Log.d("SDBFM", "Connection attempt timed out");
      return null;
    } catch (UnknownHostException e){
      Log.d("SDBFM", "Could not find the server");
      return null;
    } catch (IOException e) {
      Log.d("SDBFM", "Io exception" + e.toString());
      return null;
    }

    String send_message = mainActivity.sharedPreferences.getString("prefServerUsername", "Shark") + "|status?";
    out.print(send_message);
    out.flush();

    try {
      //Log.d("SDBFM", "Waiting for input ...");
      String return_input = in.readLine();
      Log.d("SDBFM", "Received a response:" + return_input);
      sock.close();
      return return_input;
    } catch (IOException e) {
      Log.d("SDBFM", "Could not recieve, got IOException: " + e.getMessage());
      return null;
    }

  }

  @Override
  protected void onPostExecute(String s) {
    super.onPostExecute(s);

    if (s != null){ // no error, we got actual server data back
      Log.d("SDBFM", "server data: " + s);

      //Toast.makeText(mainActivity, "Server status: Unknown", Toast.LENGTH_SHORT).show();

    }
  }
}
