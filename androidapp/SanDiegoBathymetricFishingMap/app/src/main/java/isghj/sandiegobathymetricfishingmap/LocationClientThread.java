package isghj.sandiegobathymetricfishingmap;

// Copyright 2019 Brian Klaus (isghj)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import android.widget.Toast;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.BindException;
import java.net.ConnectException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class LocationClientThread extends Thread {

  // new plan: make a thread, pass everything back to the UI thread as a bundle
  // bundle is caught with a message in teh handler
  private MainActivity  mainActivity;
  private long          startTime;

  LocationClientThread(MainActivity ourAct){
    this.mainActivity   = ourAct;
  }


  public void toast(String message){
    Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT).show();
  }

  // returns an encoded string with all location information separated by single pipes
  // x | y | speed | angle | time
  // or null if not available
  private String GetCurrentLocationString(){
    Location our_location = mainActivity.getCurrentLocation();
    if (our_location != null){
      double lastSpeed = new BigDecimal(our_location.getSpeed()*2.23694).setScale(2, RoundingMode.HALF_UP).doubleValue();
      // x | y | angle | speed | time
      return  our_location.getLongitude() + "|" +
              our_location.getLatitude() + "|" +
              (int) our_location.getBearing() + "|" +
              lastSpeed + "|" +
              new SimpleDateFormat("HH:mm:ss", Locale.US).format(new Date());
    }else
      return null;
  }

  public String RequestUpdate(String server_address, int server_port) {
    PrintWriter out;
    BufferedReader in;
    Socket sock;

    String send_message = GetCurrentLocationString();
    // break the address into parts NOW
    //int colon_count =
    //if ()
    //String ip =

    long latency = 0;
    sock = new Socket();

    try {
      startTime = System.currentTimeMillis();
      sock.bind(new InetSocketAddress(server_port));
      // TODO check if the user changed the port and put it in the server_address
      InetAddress test = InetAddress.getByName(server_address);
      if (test instanceof Inet6Address){
        //sock.connect(new InetSocketAddress(server_address, server_port), 5000);
        //Inet6Address addripv6 = Inet6Address.getByAddress(server_address, null, 0); // cannot find ANY documentation on skope_id that makes sense
        sock.connect(new InetSocketAddress(test, server_port), 5000);
      }else{
        sock.connect(new InetSocketAddress(server_address, server_port), 5000);
      }
      latency = System.currentTimeMillis() - startTime;
      out = new PrintWriter(sock.getOutputStream(), true);
      in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
    } catch (SocketTimeoutException e) {
      Log.d("SDBFM", "Connection attempt timed out");
      returnStatusToActivity("Err: SERVER TIMEDOUT");
      return null;
    } catch (UnknownHostException e) {
      Log.d("SDBFM", "Could not find the server");
      returnStatusToActivity("Err: NO ROUTE TO HOST");
      return null;
    } catch (BindException e) {
      Log.d("SDBFM", "Bind exception, Address was already in use");
      // don't think users need to know about this one, it seems to solve itself the second update
      return null;
    } catch (ConnectException e){
      Log.d("SDBFM", "Connection Exception, could not connect socket to remote");
      Log.d("SDBFM", "Message: " + e.getMessage());
      String m = e.getMessage();
      if ( m.contains("unreachable")){
        returnStatusToActivity("Err: NETWORK UNREACHABLE");
      }else if (m.contains("Invalid argument")){
        try {
          InetAddress test_inet = InetAddress.getByName(server_address);

          if (test_inet instanceof Inet6Address) {
            returnStatusToActivity("Err: CANNOT CONNECT LOCAL IPV6");
          } else {
            returnStatusToActivity("Err: INVALID ARGUMENT");
          }
        }catch (Exception f){}
      }
      return null;

    } catch (IOException e) {
      if (e.toString().contains("Connection refused")){
        Log.d("SDBFM", "Connection was refused" + e.toString());
        returnStatusToActivity("Err: CONNECTION REFUSED");
        //toast("Connection was refused");
        return null;
      }else{
        returnStatusToActivity("Err: Unknown IO Exception");
        Log.d("SDBFM", "Io exception: " + e.toString());
        return null;
      }
    }

    //Log.d("SDBFM", "Sending output: " + send_message + " ...");
    out.print(send_message);
    out.flush();

    try {
      String return_input = in.readLine();
      Log.d("SDBFM", "Received a response:" + return_input);
      sock.close();
      returnStatusToActivity(Long.toString(latency));
      return return_input;
    } catch (SocketTimeoutException e){
      returnStatusToActivity("Err: SERVER TIME OUT");
      return null;

    } catch (IOException e) {
      Log.d("SDBFM", "Could not receive, got IOException: " + e.getMessage());
      returnStatusToActivity("Err: IOException on return");
      return null;
    }

  }

  // error message from trying to reach the server
  private void returnStatusToActivity(String status){
    Message msg = new Message();
    Bundle bundle = new Bundle();

    bundle.putString("status", status);
    msg.setData(bundle);
    mainActivity.mServerHandler.sendMessage(msg);
  }

  // actual data from the server
  private void returnDataToActivity(String data_string){
    Message msg = new Message();
    Bundle bundle = new Bundle();

    bundle.putString("data", data_string);
    msg.setData(bundle);
    mainActivity.mServerHandler.sendMessage(msg);
  }

  public void run(){

    // every X seconds, we need to check the server for new user location data
    for(;;){
      try {
        String  addr = mainActivity.sharedPreferences.getString("prefServerIP", "192.168.29.82");

        int     port = 6114;
        String[] addr_parts = addr.split(":");
        if (addr_parts.length == 2){ // in the event the user adds a different port
          addr = addr_parts[0];
          port = Integer.valueOf(addr_parts[1]);
        }
        // if we have no location access, just check server status
        if (mainActivity.getCurrentLocation() == null){
          new ContactServerTask(mainActivity).execute();
        }else {
          String server_response = RequestUpdate(addr, port);
          if (server_response != null) {
            returnDataToActivity(server_response);
          }
        }
        try {
          Thread.sleep(Integer.parseInt(mainActivity.sharedPreferences.getString("prefServerUpdateFreq", "5")) * 1000);
        }catch (NumberFormatException e){
          // welp, users, can't be helped
          SharedPreferences.Editor editor = mainActivity.sharedPreferences.edit();
          editor.putString("prefServerUpdateFreq", "10");
          editor.apply();

          Thread.sleep(5000);
        }
      }catch(InterruptedException e){
        Log.d("SDBFM", "Thread was interrupted, leaving ...");
        return;
      }
    }

  }

}
