package isghj.sandiegobathymetricfishingmap;

// not convinced this should be a service, since services, it seems, are on the same thread as the mainActivity

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.IBinder;


import isghj.sandiegobathymetricfishingmap.ContactServerTask;

public class ClientLocationService extends Service {
  // these seem necessary for the class to work, we don't use them
  int startMode;
  IBinder binder;
  boolean allowRebind;
  MainActivity mainActivity;

  // actual information for us to use
  SharedPreferences ourPreferences;

  @Override
  public IBinder onBind(Intent intent){

    return binder;
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId){

    ContactServerLoop();



    return startMode;
    //mainActivity =  // we need to get this from the intent I think
  }

  @Override
  public void onDestroy(){
    // is there something we should stop/kill?
  }


  /*private boolean checkLocationServer(){

    // throws a main thread exception
    try {
      String server_check_response = new ContactServerTask(this).execute("alan|status?", "192.168.29.82").get(); // TODO username
      //toast(server_check_response);

    }catch (InterruptedException e){
      Log.d("SDBFM", "interupted exception: " + e.toString());
    }catch (ExecutionException e){
      Log.d("SDBFM", "execution exception: " + e.toString());
    }
    return false;
  }

  private boolean sendLocationData(){
    // encode our data into a string

    Location temp_loc   = mLocationOverlay.getLastFix(); // <- might be faster to use what we have then get new
    if (temp_loc == null){
      //toast("GPS hasn't found your location yet!");
    }else {
      Date time_now = new Date();
      SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.US);
      // x | y | angle | speed | time
      String our_location_string = temp_loc.getLongitude() + "|" + // TODO we don't have speed/angle yet
                                   temp_loc.getLatitude() + "|" +
                                   "0" + "|" +
                                   "1" + "|" +
                                   df.format(time_now);
      //try {
        //String server_check_response = new ContactServerTask(this).execute(our_location_string, "192.168.29.82").get();
        new ContactServerTask(this).execute(our_location_string, "192.168.29.82");
        //toast(server_check_response);
        return true;
      //} catch (InterruptedException e) {
      //  Log.d("SDBFM", "interupted exception: " + e.toString());
      //} catch (ExecutionException e) {
      //  Log.d("SDBFM", "execution exception: " + e.toString());
      //}
    }

    return false;
  }*/


  private void ContactServerLoop(){
    // if we attempt to contact server and get nothing three times in a row, we need to stop early
    // stopSelf()
    
    // TODO this wont work, because our async returns out of line, this forces us to wait
    /* boolean serverReached
    for (int i = 0; i < 3 && !serverReached; i ++){
      serverReached = checkLocationServer()
    }
    if ( ! serverReached ){
      // toast that the server failed to find anything
      stopSelf()
    } */
    
    //every X seconds, call an async task
    // I was told that this runs on the same thread as hte UID
    // so if we call an async from here, it too can do stuff do UI state
    for(;;) {
      
      
      // wait 10 seconds
    }
      
    
  }
}
