package isghj.sandiegobathymetricfishingmap;

// just using the default because I have no imagination for making this part pretty or unique
// and its a mess, android state/life cycle is not intuitive for me


import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;


public class MainPreferenceActivity extends PreferenceActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    getFragmentManager().beginTransaction().replace(android.R.id.content, new MainPreferenceFragment()).commit();

    // mothballed because we dont need it
    /*SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    sharedPreferences.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
      @Override
      public void onSharedPreferenceChanged(SharedPreferences sP, String setting_name) {
        // we need to catch the server on/off at least, the rest maybe not
        if (setting_name.equals("prefEnableServer")){
          //Log.d("SDBFM", "Our setting is: " + sP.getString("prefEnableServer", "not_a_valid_value"));
          MainActivity main = (MainActivity) get;
          if (sP.getBoolean("prefEnableServer", false) == true){
            main.startServerUpdating();

          }else{
            main.stopServerUpdating();
          }

        }
      }
    });*/


  }


  public static class MainPreferenceFragment extends PreferenceFragment  {

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.preferences_main);
    }
  }


}
